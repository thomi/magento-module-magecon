<?php

class Openlabs_OpenERPConnector_Model_Olcatalog_Products extends Mage_Catalog_Model_Api_Resource
{
    protected $_filtersMap = array(
        'product_id' => 'entity_id',
        'set'        => 'attribute_set_id',
        'type'       => 'type_id'
    );

    public function __construct()
    {
        $this->_storeIdSessionField = 'product_store_id';
        $this->_ignoredAttributeTypes[] = 'gallery';
        $this->_ignoredAttributeTypes[] = 'media_image';
    }

    /**
     * Retrieve list of products with basic info (id, sku, type, set, name)
     *
     * @param array $filters
     * @param string|int $store
     * @return array
     */
    public function items($filters = null, $store = null)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->setStoreId($this->_getStoreId($store))
            ->addAttributeToSelect('name');

        if (is_array($filters)) {
            try {
                foreach ($filters as $field => $value) {
                    if (isset($this->_filtersMap[$field])) {
                        $field = $this->_filtersMap[$field];
                    }

                    $collection->addFieldToFilter($field, $value);
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('filters_invalid', $e->getMessage());
            }
        }

        $result = array();

        foreach ($collection as $product) {
//            $result[] = $product->getData();
            $result[] = array( // Basic product data
                'product_id' => $product->getId(),
                'sku'        => $product->getSku(),
                'name'       => $product->getName(),
                'set'        => $product->getAttributeSetId(),
                'type'       => $product->getTypeId(),
                'category_ids'       => $product->getCategoryIds()
            );
        }

        return $result;
    }

    /**
     * Retrieve product info
     *
     * @param int|string $productId
     * @param string|int $store
     * @param array $attributes
     * @return array
     */
    public function biglist($productId, $store = null, $filters = null)
    {
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->setStoreId($this->_getStoreId($store))
            ->addAttributeToSelect('name');

        if (is_array($filters)) {
            try {
                foreach ($filters as $field => $value) {
                    if (isset($this->_filtersMap[$field])) {
                        $field = $this->_filtersMap[$field];
                    }

                    $collection->addFieldToFilter($field, $value);
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('filters_invalid', $e->getMessage());
            }
        }
		foreach ($collection as $product) {
	        $data = array();
	        $data = array( // Basic product data
	            'product_id' => $product->getId(),
	            'sku'        => $product->getSku(),
	            'set'        => $product->getAttributeSetId(),
	            'type'       => $product->getTypeId(),
	            'categories' => $product->getCategoryIds(),
	            'websites'   => $product->getWebsiteIds()
	        );

	        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
	                $data[$attribute->getAttributeCode()] = $product->getData($attribute->getAttributeCode());
	        }
	        $result[]=$data;
		}
        return $result;
    }

    /**
     * Create new product.
     *
     * @param string $type
     * @param int $setMage::log()
     * @param array $productData
     * @return int
     */
	public function debug($s)
	{
  		    $fp = fopen("/home/ruchir/debug.txt","a+");			
			fwrite($fp, $s."\n");
			fclose($fp);
	}
    public function remove_bundle_product($product_id){
        $product = Mage::getModel('catalog/product');
        if ($product_id) {

                        $product->load($product_id);
                    }

        Mage::log("&&&&&88888888888884444444&&");
        $bundled_product = Mage::getModel('catalog/product');
        $bundled_product->load($product->getId());

        $optionCollection = $product->getTypeInstance(true)->getOptionsCollection($product);
        $selectionCollection = $bundled_product
        ->getTypeInstance(true)
        ->getSelectionsCollection(
        $bundled_product
        ->getTypeInstance(true)
        ->getOptionsIds($bundled_product),
        $bundled_product
        );
        foreach($optionCollection as $option){
            $option->delete();
        }


    }//remove_bundle_product
    public function set_bundle_product($itemData,$selectionData,$product_id){
        $product = Mage::getModel('catalog/product');
//        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $this->remove_bundle_product($product_id);
                /*Mage::log("&&&&&888888888888888&&&&&&&&&&&".$itemData);
                Mage::log("&&&&&888888888888888&&&&&&&&&&&".$selectionData);
                Mage::log("&&&&&888888888888888&&&&&&&&&&&".$product_id);*/

  /*         foreach ($itemData as $pro1_id => $prod_data){
                Mage::log("&&&&&888888888888888&&&&&&&&&&&".$pro1_id);

                foreach ($prod_data as $pro_id => $new_prod_data){
                    Mage::log("&&&&&&&&&&&&&&&&".$pro_id);
                    $items[] = array(
                                    'title' => $new_prod_data['title'],
                                    'option_id' => $new_prod_data['option_id'],
                                    'delete' => $new_prod_data['delete'],
                                    'type' => $new_prod_data['type'],
                                    'required' => $new_prod_data['required'],
                                    'position' => $new_prod_data['position']);
                      $selections = array();
                    $selectionRawData[] = array(
                        'selection_id' => '',
                        'option_id' => '',
                        'product_id' => '44',
                        'delete' => '',
                        'selection_price_value' => '10',
                        'selection_price_type' => 0,
                        'selection_qty' => 1,
                        'selection_can_change_qty' => 0,
                        'position' => 0);
                    $selections[] = $selectionRawData;
                    foreach($new_prod_data['product_data'] as$lid => $new_list){
                        Mage::log("lidlidlidlidlidlid".$lid);

                            $selections[] = $new_list;
                            
                            Mage::log("&&&&&&&&&&&&&&&&555555555555555555555".$selectionData);
                                foreach($selectionData as $rt=>$tr){
                                    foreach($tr as $tt=>$ty){
                                                Mage::log("000000003 ".$tt);
                                                Mage::log("000000002 ".$ty);}
                                 }
                    
                    Mage::log("&&&&&&&&&&&&&&&&0");*/
                  
            if ($product_id) {
//                $product = $this->_getProduct($product_id);

                $product->load($product_id);
            }


            Mage::register('product', $product);
//            Mage::register('current_product', $product);
            Mage::log("&&&&&&&&&&&&&&&&0");
            $product->setCanSaveConfigurableAttributes(false);
            $product->setCanSaveCustomOptions(true);

            $product->setBundleOptionsData($itemData);
            $product->setBundleSelectionsData($selectionData);
            $product->setCanSaveBundleSelections(true);
            $product->setAffectBundleProductSelections(true);


            $product->save();
                        
                
           
    }


                        /*foreach($new_list as $new_data){
                            Mage::log("new_datanew_datanew_data".$new_data);
                            $selectionRawData[] = array();
                            'selection_id' => $new_data['selection_id'],
                            'option_id' => $new_data['option_id'],
                            'product_id' => $new_data['product_id'],
                            'delete' => $new_data['delete'],
                            'selection_price_value' => $new_data['selection_price_value'],
                            'selection_price_type' => $new_data['selection_price_type'],
                            'selection_qty' => $new_data['selection_qty'],
                            'selection_can_change_qty' => $new_data['selection_can_change_qty'],
                            'position' => $new_data['position']);*/
/*           $items[] = array(
                'title' => 'test title',
                'option_id' => '',
                'delete' => '',bundle
                'type' => 'radio',
                'required' => 1,
                'position' => 0);

            $selections = array();
            $selectionRawData[] = array(
                'selection_id' => '',
                'option_id' => '',
                'product_id' => '39',
                'delete' => '',
                'selection_price_value' => '10',
                'selection_price_type' => 0,
                'selection_qty' => 1,
                'selection_can_change_qty' => 0,
                'position' => 0);
            $selections[] = $selectionRawData;

            $productId = 38;
            $product    = Mage::getModel('catalog/product')
            ->setStoreId(0);
            if ($productId) {
                $product->load($productId);
            }
            Mage::register('product', $product);
            Mage::register('current_product', $product);
            $product->setCanSaveConfigurableAttributes(false);
            $product->setCanSaveCustomOptions(true);

            $product->setBundleOptionsData($items);
            $product->setBundleSelectionsData($selections);
            $product->setCanSaveCustomOptions(true);
            $product->setCanSaveBundleSelections(true);

            $product->save();*/

    
   
    public function create($type, $set, $sku, $productData)
    {
        if (!$type || !$set || !$sku) {
            $this->_fault('data_invalid');
        }

        $product = Mage::getModel('catalog/product');
        /* @var $product Mage_Catalog_Model_Product */
        $product->setStoreId($this->_getStoreId($store))
            ->setAttributeSetId($set)
            ->setTypeId($type)
            ->setSku($sku);

        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute)
                && isset($productData[$attribute->getAttributeCode()])) {
                $product->setData(
                    $attribute->getAttributeCode(),
                    $productData[$attribute->getAttributeCode()]
                );
            }
        }

        $this->_prepareDataForSave($product, $productData);

        if (is_array($errors = $product->validate())) {
            $this->_fault('data_invalid', implode("\n", $errors));
        }

        try {
            $product->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
	if(array_key_exists('config_attribute_id',$productData)){
		$resource = Mage::getSingleton('core/resource')
		    			->getConnection('core_write');
		$res = Mage::getSingleton('core/resource');
		$tabname = $res->getTableName('catalog_product_super_attribute');
		$link_tab = $res->getTableName('catalog_product_super_link');
		$resource->query("insert into " . $tabname . "(product_id, attribute_id, position) values (" . $product->getId() . "," . $productData['config_attribute_id']. ", 0)");
		foreach ($productData['associated'] as $asso){
			$resource->query("insert into " . $link_tab . "(product_id, parent_id) values (". $asso. ",". $product->getId(). ")");
		}
	}

        return $product->getId();
    }

    /**
     * Update product data
     *
     * @param int|string $productId
     * @param array $productData
     * @param string|int $store
     * @return boolean
     */
    public function update($productId, $productData = array(), $store = null,$bundle=false)
    {
        $product = $this->_getProduct($productId, $store);

        if (!$product->getId()) {
            $this->_fault('not_exists');
        }

        if (isset($productData['website_ids']) && is_array($productData['website_ids'])) {
            $product->setWebsiteIds($productData['website_ids']);
        }

        foreach ($product->getTypeInstance(true)->getEditableAttributes($product) as $attribute) {
            if ($this->_isAllowedAttribute($attribute)
                && isset($productData[$attribute->getAttributeCode()])) {
                $product->setData(
                    $attribute->getAttributeCode(),
                    $productData[$attribute->getAttributeCode()]
                );
            }
        }

        $this->_prepareDataForSave($product, $productData);

        try {
            if (is_array($errors = $product->validate())) {
                $this->_fault('data_invalid', implode("\n", $errors));
            }
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }

        try {
            $product->save();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('data_invalid', $e->getMessage());
        }
	if(array_key_exists('config_attribute_id',$productData)){
		$resource = Mage::getSingleton('core/resource')
		    			->getConnection('core_write');
		$res = Mage::getSingleton('core/resource');
		$tabname = $res->getTableName('catalog_product_super_attribute');
		$link_tab = $res->getTableName('catalog_product_super_link');
		$resource->query("DELETE from " . $tabname . " where product_id = " . $product->getId() );
		$resource->query("insert into " . $tabname . "(product_id, attribute_id, position) values (" . $product->getId() . "," . $productData['config_attribute_id']. ", 0)");
		$resource->query("DELETE from " . $link_tab . " where parent_id = " . $product->getId() );
		foreach ($productData['associated'] as $asso){
			$resource->query("insert into " . $link_tab . "(product_id, parent_id) values (". $asso. ",". $product->getId(). ")");
		}
	}

        return true;
    }

    /**
     *  Set additional data before product saved
     *
     *  @param    Mage_Catalog_Model_Product $product
     *  @param    array $productData
     *  @return	  object
     */
    protected function _prepareDataForSave ($product, $productData)
    {
        if (isset($productData['categories']) && is_array($productData['categories'])) {
            $product->setCategoryIds($productData['categories']);
        }

        if (isset($productData['websites']) && is_array($productData['websites'])) {
            foreach ($productData['websites'] as &$website) {
                if (is_string($website)) {
                    try {
                        $website = Mage::app()->getWebsite($website)->getId();
                    } catch (Exception $e) { }
                }
            }
            $product->setWebsiteIds($productData['websites']);
        }

        if (isset($productData['stock_data']) && is_array($productData['stock_data'])) {
            $product->setStockData($productData['stock_data']);
        }
    }

    /**
     * Update product special price
     *
     * @param int|string $productId
     * @param float $specialPrice
     * @param string $fromDate
     * @param string $toDate
     * @param string|int $store
     * @return boolean
     */
    public function setSpecialPrice($productId, $specialPrice = null, $fromDate = null, $toDate = null, $store = null)
    {
        return $this->update($productId, array(
            'special_price'     => $specialPrice,
            'special_from_date' => $fromDate,
            'special_to_date'   => $toDate
        ), $store);
    }

    /**
     * Retrieve product special price
     *
     * @param int|string $productId
     * @param string|int $store
     * @return array
     */
    public function getSpecialPrice($productId, $store = null)
    {
        return $this->info($productId, $store, array('special_price', 'special_from_date', 'special_to_date'));
    }

    /**
     * Delete product
     *
     * @param int|string $productId
     * @return boolean
     */
    public function delete($productId)
    {
        $product = $this->_getProduct($productId);

        if (!$product->getId()) {
            $this->_fault('not_exists');
        }

        try {
            $product->delete();
        } catch (Mage_Core_Exception $e) {
            $this->_fault('not_deleted', $e->getMessage());
        }

        return true;
    }
} // Class Mage_Catalog_Model_Product_Api End
